import { Test, TestingModule } from '@nestjs/testing';
import { GbhrService } from './gbhr.service';

describe('GbhrService', () => {
  let service: GbhrService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GbhrService],
    }).compile();

    service = module.get<GbhrService>(GbhrService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
