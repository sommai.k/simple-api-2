import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { Gbhr } from 'src/entities/gbhr.entity';
import { GbHrDto } from './dto/gbhr.dto';
import { GbhrService } from './gbhr.service';

@Controller('gbhr')
export class GbhrController {
  logger: Logger = new Logger(GbhrController.name);

  constructor(private gbhrService: GbhrService) {}

  @Get()
  findAll(@Query('start') start: string, @Query('size') size: string) {
    return this.gbhrService.findAll(start, size);
  }

  @Post()
  async createHr(@Body() hr: GbHrDto) {
    // console.log('this is a console');
    this.logger.log('from browser');
    this.logger.log(hr);
    // return this.gbhrService.create(hr);
    return this.gbhrService.simpleCreate(hr);
  }

  @Put()
  updateHr(@Query('id') id: number, @Body() hr: GbHrDto) {
    return this.gbhrService.simpleUpdate(id, hr);
  }

  @Delete()
  deleteHr(@Query('id') id: number) {
    return this.gbhrService.simpleDelete(id);
  }
}
