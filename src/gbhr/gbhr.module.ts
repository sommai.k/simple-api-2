import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Gbhr } from 'src/entities/gbhr.entity';
import { GbhrController } from './gbhr.controller';
import { GbhrService } from './gbhr.service';

@Module({
  imports: [TypeOrmModule.forFeature([Gbhr])],
  controllers: [GbhrController],
  providers: [GbhrService],
})
export class GbhrModule {}
