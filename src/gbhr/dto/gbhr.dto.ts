export interface GbHrDto {
  code: string;
  name: string;
  title: string;
}
