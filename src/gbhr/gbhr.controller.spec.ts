import { Test, TestingModule } from '@nestjs/testing';
import { GbhrController } from './gbhr.controller';

describe('GbhrController', () => {
  let controller: GbhrController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GbhrController],
    }).compile();

    controller = module.get<GbhrController>(GbhrController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
