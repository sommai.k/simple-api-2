import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Gbhr } from 'src/entities/gbhr.entity';
import { Repository } from 'typeorm';
import { GbHrDto } from './dto/gbhr.dto';

@Injectable()
export class GbhrService {
  constructor(
    @InjectRepository(Gbhr)
    private gbhrRepo: Repository<Gbhr>,
  ) {}

  findAll(start: string, size: string) {
    const end = parseInt(start) + parseInt(size);
    return this.gbhrRepo.query(`
    select *
      from (
        select t.*, rownum rn
        from (
          SELECT id as "id", code as "code", name as "name", title as "title"
          FROM afr.t_gb_hr
          ORDER BY id
      ) t
      where rownum <= ${end}) f
    where rn > ${start}  
    `);
    // return this.gbhrRepo.find();
  }

  async create(dto: GbHrDto) {
    const gbhr: Gbhr = this.gbhrRepo.create();
    gbhr.code = dto.code;
    gbhr.name = dto.name;
    gbhr.createDate = new Date();
    gbhr.title = dto.title;
    const seq = await this.gbhrRepo.query(
      'select afr.t_gb_hr_seq.nextval from dual',
    );
    gbhr.id = seq[0].NEXTVAL;
    console.log(seq);
    return this.gbhrRepo.insert(gbhr);
    // return this.gbhrRepo.save(gbhr);
  }

  async simpleCreate(dto: GbHrDto) {
    const seq = await this.gbhrRepo.query(
      'select afr.t_gb_hr_seq.nextval from dual',
    );

    const sql = `
      insert into afr.t_gb_hr(id, code, name, title)
      values(
        ${seq[0].NEXTVAL}, 
        '${dto.code}',
        '${dto.name}',
        '${dto.title}'
      )
    `;

    return this.gbhrRepo.query(sql);
  }

  async simpleUpdate(id: number, dto: GbHrDto) {
    const sql = `
      update afr.t_gb_hr
        set code = '${dto.code}'
          , name = '${dto.name}'
          , title = '${dto.title}'
      where id = ${id}
    `;
    return this.gbhrRepo.query(sql);
  }

  async simpleDelete(id: number) {
    const sql = `
      delete from afr.t_gb_hr
      where id = ${id}
    `;
    return this.gbhrRepo.query(sql);
  }
}
