import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GbhrModule } from './gbhr/gbhr.module';
import * as config from './ormconfig';

@Module({
  imports: [TypeOrmModule.forRoot(config), GbhrModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
