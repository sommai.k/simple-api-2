import { ConnectionOptions } from 'typeorm';

// this config file for migration cli
const config: ConnectionOptions = {
  type: 'oracle',
  host: 'dboda-scan.rubber.co.th',
  port: 1521,
  username: 'train00',
  password: 'train00',
  serviceName: 'testrac',
  entities: [__dirname + '/**/*.entity{.ts,.js}'],
  extra: { timezone: '+07' },

  // We are using migrations, synchronize should be set to false.
  synchronize: false,

  // Run migrations automatically,
  // you can disable this if you prefer running migration manually.
  migrationsRun: false,
  logging: true,
  //   logger: 'file',

  // Allow both start:prod and start:dev to use migrations
  // __dirname is either dist or src folder, meaning either
  // the compiled js in prod or the ts in dev.
  migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
  cli: {
    // Location of migration should be inside src folder
    // to be compiled into dist/ folder.
    migrationsDir: 'src/migrations',
  },
};

export = config;
