import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'afr.t_gb_hr' })
export class Gbhr {
  @PrimaryColumn({ name: 'id' })
  id: number;

  @Column({ name: 'code' })
  code: string;

  @Column({ name: 'name' })
  name: string;

  @Column({ name: 'title' })
  title: string;

  @Column({ name: 'create_date' })
  createDate: Date;
}
